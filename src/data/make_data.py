import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.collections import PolyCollection
from mpl_toolkits.mplot3d import Axes3D
from scipy import signal
from scipy import stats
import scipy
from toolz import compose, partial



def butter_filter(input_signal,N,
                  cutoff,fs,btype='high',
                  analog=False,output='sos',
                  check_response=False):
    if output=='zpk':
        raise Exception('Zero and poles output is not implemented in this version! \
                       You must use "ba" or "sos" output.')    
    
    nyq = 0.5 * fs
    Wn = np.asarray(cutoff)/nyq

    butter_coef = signal.butter(N,Wn,btype=btype, analog=analog,output=output)
    
    if check_response:
        check_filter_response(butter_coef, fs)
    
    if output=='sos':
        return signal.sosfiltfilt(butter_coef, input_signal,padlen=int(N/2),padtype ='even')
    
    elif output=='ba':
        return signal.filtfilt(butter_coef[0],butter_coef[1], input_signal)


def acc_to_vel(input_list, fs, cutoff=0, order=0, convert=False):
    '''Acceleration to velocity conversion.'''
    input_array = pd.Series(input_list)
    if convert:
        convert = 9.81/0.001 # g to mm/s^2
        input_array = convert*input_array
    zero_mean_input_array = input_array - input_array.mean()
    integral = zero_mean_input_array.cumsum()
    velocity = integral / fs
    velocity_zero_mean = velocity - velocity.mean()
    
    if cutoff != 0 and order != 0:
        return_array =  butter_filter(velocity_zero_mean,order,cutoff,fs)
    else:
        return_array = velocity_zero_mean
    return list(return_array)



def make_dataset_processed(df):
    samplingRate = []
    for i in range(len(df)):
        samplingRate.append(df.spectralSettings[i]['samplingRate'])
    df['samplingRate'] = samplingRate
    acc_to_vel_fs = partial(acc_to_vel, fs=df['samplingRate'])
    df['X_velocity'] = df['X'].apply(acc_to_vel_fs)
    df['Y_velocity'] = df['Y'].apply(acc_to_vel_fs)
    df['Z_velocity'] = df['Z'].apply(acc_to_vel_fs)
    
    return df
    
