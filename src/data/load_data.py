import sys
import pickle
import pandas as pd
import glob


def load_datasets(data_source='spectral'):
    dfs = {}
    df_result = pd.DataFrame()
    for direc in glob.glob(f"../data/raw/{data_source}/*"):
        with open(f"{direc}", 'rb') as f:
            dfs[direc.split('/')[-1].split('.')[0]] = pickle.load(f)
        df_part = dfs[direc.split('/')[-1].split('.')[0]]
        df_part['data_source'] = direc.split('/')[-1].split('.')[0]
        df_result = pd.concat([df_part, df_result])
    return df_result.reset_index(drop=True)



