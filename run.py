#!/usr/bin/python3.6

import argparse
import os
import pandas as pd
from src.features import build_features as bf
from src.models import predict_model as pm
from src.models import train_model as tm


def execute_scripts(args):

    if args.type == 'train':

        print(bf.make_data_train())
        print('start train model')
        print(tm.train_model())
    
    elif args.type == 'predict':
        print(bf.make_data_predict())
        print('start predict model')
        print(pm.predict_model())
    else:
        print('parametro nao encontrado')
    
    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--type", help="set train or predict")
    
    args = parser.parse_args()

    execute_scripts(args)



