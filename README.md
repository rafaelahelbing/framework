
# ML Framework
Machine Learning Framework para Data Science foi elaborado com o objetivo de padronizar os projetos de modelagem

---

### 1. Boas Práticas


#### 1.1. Formatter

Favor sempre formatar o código. Use [Black](https://github.com/ambv/black).

```
(venv) $ pip install black
```

Rodar o comando:

```
(venv) $ black -l 79 /path/to/file.py  # or
(venv) $ black -l 79 /path/to/directory/
```
---
# [Nome do Projeto]

---

## 1. Estrutura do Repositório


```

├── README.md          <- O top-level README para desenvolvedores que utilizam este projeto.
├── data
│   ├── processed      <- Datasets finais para modelagem, dado pós processamento.
│   ├── raw            <- Datasets originais sem processamento. 
│   └── result         <- Datasets com resultados das previsões 
│
├── docs               <- Documentação final do projeto
│
├── models             <- Modelos treinados e serializados.
│
├── notebooks          <- Jupyter notebooks. A convenção de nomenclatura é um número (para ordenação),
│                         e uma breve descrição delimitada por `-`, por exemplo: 
│                         `1.0-initial-data-exploration`.
│
├── reports           
│   ├── data-infos     <- Reporte da qualidade, dicionario de dados e relação entre tabelas.
│   └── model-results  <- Resumo da performance dos modelos.
│
├── requirements.txt   <- Arquivo de requisitos para reproduzir o ambiente de análise, 
│                         pode ser gerado com `pip freeze > requirements.txt`
│ 
├── sandbox            <- Diretório destinado para ser um ambiente de desenvolvimento, onde todos os jupyter's ou
│                         .py's intermediários podem ser armazenados durante o decorrer do projeto, para entrega
│                         final este diretório deve ser excluído.
│
├── run.py             <- Script para executar o os modelos.
│
└──── src              <- Código-fonte para uso neste projeto.
    │
    ├── data           <- Scripts para baixar ou gerar dados
    │   └── make_dataset.py
    │
    ├── features       <- Scripts para transformar dados brutos em recursos para modelagem
    │   └── build_features.py
    │
    └── models         <- Scripts para treinar modelos e utilizar modelos treinados para fazer previsões
        ├── predict_model.py
        └── train_model.py

```
---

## 2. Reproduzir o Projeto

Passo a passo para reproduzir este projeto em seu ambiente:

### 2.1. Instalar o ambiente (Virtual Env)

Para desenvolvimento do projeto, é fundamental que o ambiente esteja isolado, para isso é necessário criar um virtual env, abaixo passo a passo:

**Instalação** 

On macOS and Linux:

`python3 -m pip install --user virtualenv`

On Windows:

`py -m pip install --user virtualenv`

**Criação do Ambiente** 

No diretório raiz do projeto, criar o ambiente virtual:

On macOS and Linux:

`python3 -m venv env`

On Windows:

`py -m venv env`

**Ativação do Ambiente** 

On macOS and Linux:

`source env/bin/activate`

**Instalação das dependências do projeto** 

Para instalar todos os pacotes python usados no projeto, ainda na raiz do projeto:

```
(venv) $ pip install -r requirements.txt
```


### 2.2. Execução do Modelo


No diretório raiz do projeto, ativar o `virtualenv`

```
$ source env/bin/activate
```

Para treinar os modelos:

`(venv) $ python3 run.py --type train`

Para usar os modelos treinados para previsão:

`(venv) $ python3 run.py --type predict`

Principal documentação em:

`doc/1.0-doc-final.html`

On Windows:

`.\env\Scripts\activate`

**Instalação de pacotes python** 

Caso precise instalar manualmente algum pacote, com o ambiente ativo, executar o comando:

`pip install nome-pacote`

**Desativar Ambiente** 

`deactivate`









